name = "Gab"
age = 26
occupation = "developer"
movie = "Inception"
rating = 92.68

print(f"I am {name}, and I am {age} years old, I work as a {occupation}, and my rating for {movie} is {rating}%")

num1, num2, num3 = 2, 75, 150

print(num1 * num2)
print(num1 < num3)
num2 += num3
print(num2)